import 'package:api_test_3/src/model/siswa.dart';
import 'dart:async';
import 'package:http/http.dart' as http;

class ApiService {
  static const url = "https://api-v2.pondokdiya.id";

  Future<List<Siswa>> getAll() async {
    final response = await http.get("$url/siswa");
    if (response.statusCode == 200) {
      return siswaFromJsonAll(response.body);
    } else {
      throw Exception('Uuuuups');
    }
  }

  Future<bool> post(Siswa data) async {
    final response = await http.post('$url/add-siswa',
        headers: {"content-type": "application/json"}, body: dataToJson(data));
    print(response.statusCode);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> update(Siswa data, int id) async {
    final response = await http.put('$url/siswa/$id',
        headers: {"content-type": "application/json"}, body: dataToJson(data));
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> delete(int id) async {
    final response = await http.delete('$url/delete-siswa/$id',
        headers: {"content-type": "application/json"});
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
