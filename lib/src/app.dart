import 'package:flutter/material.dart';
import 'package:api_test_3/src/ui/list_siswa.dart';
import 'package:api_test_3/src/ui/formadd.dart';

GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        key: _scaffoldState,
        appBar: AppBar(
          title: Text("List Siswa"),
          actions: <Widget>[
            GestureDetector(
              onTap: () async {
                var result = await Navigator.push(_scaffoldState.currentContext,
                    MaterialPageRoute(builder: (BuildContext context) {
                  return FormAdd();
                }));

                if (result != null) {
                  setState(() {});
                }
              },
              child: Icon(Icons.add, color: Colors.white),
            )
          ],
        ),
        body: ListSiswa(),
      ),
    );
  }
}
