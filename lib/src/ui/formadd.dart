import 'package:flutter/material.dart';
import 'package:api_test_3/src/api/apiService.dart';
import 'package:api_test_3/src/model/siswa.dart';

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class FormAdd extends StatefulWidget {
  @override
  _FormAddState createState() => _FormAddState();
}

class _FormAddState extends State<FormAdd> {
  ApiService apiService = ApiService();
  TextEditingController _nama = TextEditingController();
  TextEditingController _alamat = TextEditingController();
  TextEditingController _t_lahir = TextEditingController();
  TextEditingController _jl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,
      appBar: AppBar(
        title: Text("Input Data"),
      ),
      body: Container(
        child: Padding(
          padding: EdgeInsets.all(20.0),
          child: ListView(
            children: <Widget>[
              TextField(
                autocorrect: false,
                decoration: InputDecoration(labelText: "Nama"),
                controller: _nama,
              ),
              TextField(
                autocorrect: false,
                decoration: InputDecoration(labelText: "Alamat"),
                controller: _alamat,
              ),
              TextField(
                autocorrect: false,
                decoration: InputDecoration(labelText: "Tanggal Lahir"),
                controller: _t_lahir,
              ),
              TextField(
                autocorrect: false,
                decoration: InputDecoration(labelText: "Jenis Kelamin"),
                controller: _jl,
              ),
              RaisedButton(
                child: Text("Simpan"),
                onPressed: () {
                  String nama = _nama.text.toString();
                  String alamat = _alamat.text.toString();
                  String t_lahir = _t_lahir.text.toString();
                  String jl = _jl.text.toString();
                  Siswa siswa = new Siswa(
                      id: 0,
                      nama: nama,
                      alamat: alamat,
                      t_lahir: t_lahir,
                      jl: jl);
                  apiService.post(siswa).then((success) {
                    print(success);
                    Navigator.pop(_scaffoldState.currentState.context, true);
                  });
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
