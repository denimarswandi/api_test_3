import 'package:flutter/material.dart';
import 'package:api_test_3/src/model/siswa.dart';
import 'package:api_test_3/src/api/apiService.dart';

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class UpdateData extends StatefulWidget {
  Siswa siswa;
  UpdateData({Key key, @required this.siswa}) : super(key: key);

  @override
  _UpdateDataState createState() => _UpdateDataState();
}

class _UpdateDataState extends State<UpdateData> {
  TextEditingController _nama = TextEditingController();
  TextEditingController _alamat = TextEditingController();
  TextEditingController _t_lahir = TextEditingController();
  TextEditingController _jl = TextEditingController();
  ApiService apiService = ApiService();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nama.text = widget.siswa.nama;
    _alamat.text = widget.siswa.alamat;
    _t_lahir.text = widget.siswa.t_lahir;
    _jl.text = widget.siswa.jl;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,
      appBar: AppBar(
        title: Text("Update Data"),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            TextField(
                autocorrect: false,
                decoration: InputDecoration(labelText: "Nama"),
                controller: _nama),
            TextField(
                autocorrect: false,
                decoration: InputDecoration(labelText: "Alamat"),
                controller: _alamat),
            TextField(
                autocorrect: false,
                decoration: InputDecoration(labelText: "Tanggal Lahir"),
                controller: _t_lahir),
            TextField(
                autocorrect: false,
                decoration: InputDecoration(labelText: "Jenis Kelamin"),
                controller: _jl),
            RaisedButton(
              child: Text("Update"),
              onPressed: () {
                Siswa siswa__ = Siswa(
                    id: 0,
                    nama: _nama.text,
                    alamat: _alamat.text,
                    t_lahir: _t_lahir.text,
                    jl: _jl.text);
                apiService.update(siswa__, widget.siswa.id).then((value) {
                  if (value) {
                    Navigator.pop(_scaffoldState.currentState.context, true);
                  }
                });
              },
            )
          ],
        ),
      ),
    );
  }
}
