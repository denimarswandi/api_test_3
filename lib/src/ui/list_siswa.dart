import 'package:flutter/material.dart';
import 'package:api_test_3/src/api/apiService.dart';
import 'package:api_test_3/src/model/siswa.dart';
import 'package:api_test_3/src/ui/updateData.dart';

class ListSiswa extends StatefulWidget {
  @override
  _ListSiswaState createState() => _ListSiswaState();
}

class _ListSiswaState extends State<ListSiswa> {
  ApiService apiService;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    apiService = ApiService();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: FutureBuilder(
        future: apiService.getAll(),
        builder: (BuildContext context, AsyncSnapshot<List<Siswa>> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text("${snapshot.error.toString()}"),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            List<Siswa> siswa = snapshot.data;
            return _buildListView(siswa);
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Widget _buildListView(List<Siswa> _siswa) {
    return Container(
        child: Column(children: <Widget>[
      Container(
        padding: EdgeInsets.only(top: 20, right: 20, left: 20),
        height: 100,
        width: MediaQuery.of(context).size.width,
        child: Card(
          semanticContainer: true,
          child: Text(
            "haloo",
            style: TextStyle(fontSize: 40.0),
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(bottom: 16.0),
      ),
      Expanded(
          child: ListView.builder(
        itemCount: _siswa.length,
        itemBuilder: (context, index) {
          Siswa __siswa = _siswa[index];
          return Card(
              child: Column(
            children: <Widget>[
              Text(__siswa.nama.toString()),
              Text(__siswa.alamat.toString()),
              Text(__siswa.t_lahir.toString()),
              Text(__siswa.jl.toString()),
              RaisedButton(
                child: Text("Hapus"),
                onPressed: () {
                  apiService.delete(__siswa.id).then((value) {
                    setState(() {});
                  });
                },
              ),
              RaisedButton(
                child: Text("Update"),
                onPressed: () async {
                  var result = await Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext context) {
                    return UpdateData(siswa: _siswa[index]);
                  }));

                  if (result != null) {
                    setState(() {});
                  }
                },
              )
            ],
          ));
        },
      ))
    ]));
  }
}
